<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/request', 'LeaveController@request')->name('request');
Route::post('/request', 'LeaveController@postrequest')->name('postrequest');
Route::get('/leaves', 'LeaveController@leaves')->name('leaves');
Route::get('/leaves/show/{id}', 'LeaveController@showrequest')->name('showrequest');
Route::get('/leaves/accept/{id}', 'LeaveController@accept')->name('accept');
Route::get('/leaves/reject/{id}', 'LeaveController@reject')->name('reject');
Route::get('/leaves/myleaves', 'LeaveController@myleaves')->name('myleaves');

Route::get('/users', 'HomeController@users')->name('users');
Route::get('/users/remove/{id}', 'HomeController@removeuser')->name('removeuser');
Route::get('/users/{id}/profile', 'HomeController@profile')->name('profile');
Route::post('/users/roles', 'HomeController@postrole')->name('postrole');

Route::post('/users/profile/update', 'HomeController@updateprofile')->name('updateprofile');
Route::get('/user/profile', 'HomeController@userprofile')->name('userprofile');

Route::get('/leaves/pending', 'LeaveController@pending')->name('pending');
Route::get('/leaves/accepted', 'LeaveController@accepted')->name('accepted');
Route::get('/leaves/rejected', 'LeaveController@rejected')->name('rejected');


