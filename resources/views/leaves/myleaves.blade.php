@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-header">My leave History</div>
		<div class="card-body">
			<table class="table table-striped">
				<thead>
				<th>#</th>
				<th>Type</th>
				<th>Staring date</th>
				<th>End date</th>
				<th>Sent</th>
				<th>Status</th>
				<th></th>
				</thead>
				<tbody>
				@if(count($leaves)>0)
					@foreach($leaves as $leave)
						<tr>
							<td>{{$leave->id}}</td>
							<td>{{$leave->type}}</td>
							<td>{{$leave->startdate}}</td>
							<td>{{$leave->enddate}}</td>
							<td>{{$leave->created_at->diffForHumans()}}</td>
							<td>{{$leave->status}}</td>
							@if(($leave->status)=='Pending')
								<td><a class="btn btn-primary btn-sm fa fa-sync">Pending</a> </td>
							@elseif(($leave->status)=='Rejected')
								<td><a class="btn btn-danger btn-sm fa fa-cut" >Rejected</a> </td>
							@elseif(($leave->status)=='Accepted')
								<td><a class="btn btn-success btn-sm fa fa-check">Accepted</a> </td>

							@endif
						</tr>
					@endforeach
				@else
					<p>No Pending leaves</p>
				@endif
				</tbody>
				{{$leaves->links()}}
			</table>
		</div>
	</div>
@endsection
