@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-header">Leaves</div>
		<div class="card-body">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link active fa fa-baby" style="color: grey;" href="{{route('leaves')}}">All leaves</a>
				</li>
				<li class="nav-item">
					<a class="nav-link fa fa-sync" href="{{route('pending')}}">Pending Leaves</a>
				</li>
				<li class="nav-item">
					<a class="nav-link fa fa-check" style="color: green;" href="{{route('accepted')}}">Accepted leaves</a>
				</li>
				<li class="nav-item">
					<a class="nav-link fa fa-cut" style="color: red" href="{{route('rejected')}}">Rejected leaves</a>
				</li>
			</ul>
			<table class="table table-striped">
				<thead>
				<th>#</th>
				<th>Type</th>
				<th>Staring date</th>
				<th>End date</th>
				<th>Sent</th>
				<th>Sender</th>
				<th>Status</th>
				<th></th>
				</thead>
				<tbody id="myTable">
				@if(count($leaves)>0)
					@foreach($leaves as $key => $leave)
						<tr>
							<td>{{$key+1}}</td>
							<td>{{$leave->type}}</td>
							<td>{{$leave->startdate}}</td>
							<td>{{$leave->enddate}}</td>
							<td>{{$leave->created_at->diffForHumans()}}</td>
							<td><a href="{{route('profile', $leave->id)}}">{{$leave->user->name}}</a></td>
							<td>{{$leave->status}}</td>
							@if(($leave->status)=='Pending')
							<td><a class="btn btn-primary btn-sm fa fa-sync" href="{{route('showrequest',['id'=>$leave->id])}}">Pending</a> </td>
								@elseif(($leave->status)=='Rejected')
								<td><a class="btn btn-danger btn-sm fa fa-cut" href="{{route('showrequest',['id'=>$leave->id])}}">Rejected</a> </td>
							@elseif(($leave->status)=='Accepted')
								<td><a class="btn btn-success btn-sm fa fa-check" href="{{route('showrequest',['id'=>$leave->id])}}">Accepted</a> </td>

								@endif
						</tr>
					@endforeach
				@else
					<p>No Pending leaves</p>
				@endif
				</tbody>
				{{$leaves->links()}}
			</table>
		</div>
	</div>
@endsection
