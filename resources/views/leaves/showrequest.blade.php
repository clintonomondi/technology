@extends('layouts.app')

@section('content')

	<div class="row justify-content-center">
		@include('includes.message')
		<div class="col-sm-6 col-sm-4">
			<div class="feature-block">
				<span class="fa fa-pull-left"> <h2>{{$sender->name}}</h2></span>
				<span class="fa fa-pull-right"> <h2>{{$leave->created_at->diffForHumans()}}</h2></span>
				<hr><br>
				<p>{{$leave->reason}}</p>
				@if(($leave->status)=='Pending')
				<div class="progress">
					<div class="progress-bar progress-bar-striped progress-bar-animated" style="width:50%">Pending</div>
				</div>

					<hr>
				<div>
					<a class="btn btn-outline-success btn-sm fa fa-check" href="{{route('accept',['id'=>$leave->id])}}">Accept</a>
					<a class="btn btn-outline-danger btn-sm fa fa-cut" href="{{route('reject',['id'=>$leave->id])}}">Reject</a>
					@elseif(($leave->status)=='Rejected')
						<div class="progress">
							<div class="progress-bar bg-danger " style="width:100%">Rejected</div>
						</div>
					@elseif(($leave->status)=='Accepted')
						<div class="progress">
							<div class="progress-bar bg-success " style="width:100%">Accepted</div>
					@endif

			</div>
		</div>
	</div>
@endsection
