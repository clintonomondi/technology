@extends('layouts.app')

@section('content')
	<div class="row justify-content-center">
		<div class="col-sm-6 col-sm-6">
		@include('includes.message')
				@if(($leave)>0)
					<marquee><p style="color: red">You have Pending leave and not legible to apply for another leave</p></marquee>
				@endif
		<form method="post" action="{{route('postrequest')}}">
			@csrf
			<div class="form-group">
				<label for="sel1">App type:</label>
				<select class="form-control" id="sel1" name="type">
					<option></option>
					<option>Education</option>
					<option>Sick</option>
					<option>Maternal</option>
					<option>Emergency</option>
				</select>
			</div>
			<div class="form-group">
				<label for="email">Start date:</label>
				<input type="date" class="form-control" id="email" name="startdate">
			</div>
			<div class="form-group">
				<label for="pwd">End date:</label>
				<input type="date" class="form-control" id="pwd" name="enddate">
			</div>
			<div class="form-group">
				<label for="comment">Reason:</label>
				<textarea class="form-control" rows="5" id="comment" name="reason"></textarea>
			</div>
			<input type="text" name="status" value="Pending" hidden>
			<div class="form-group form-check">
				<label class="form-check-label">

				</label>
			</div>
			<button type="submit" class="btn btn-outline-info btn-sm">Request</button>
		</form>
		</div>
	</div>
@endsection
