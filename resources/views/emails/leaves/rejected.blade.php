@component('mail::message')
# Leave System:Admin

Greatings! WE received your request for a leave,unfortunately we are not able to offer you a leave at this time
Please contact the admin for further info.

@component('mail::button', ['url' => 'http://technology.test/login'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
