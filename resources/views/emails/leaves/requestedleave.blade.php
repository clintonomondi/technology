@component('mail::message')
# Leave Management

Hello,this is to notify you that an employee is requesting for a leave
Kindly login to manage.

@component('mail::button', ['url' => 'http://technology.test/login'])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
