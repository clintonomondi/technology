@component('mail::message')
# Leave management:Admin

Greatings! This is to notify you that the leave you requested for has been approved.

@component('mail::button', ['url' => 'http://technology.test/login'])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
