@extends('layouts.app')

@section('content')
		<div class="row justify-content-center">
			<div class="profile-header-container">
				<div class="profile-header-img">
					@include('includes.message')
					<img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" style="height: 200px;width: 200px;" />
					<!-- badge -->
					<div class="rank-label-container">
						<span class="label label-default rank-label">{{$user->name}}</span>
					</div>
				</div>
			</div>

		</div>
		<div class="row justify-content-center">
			<form action="{{route('updateprofile')}}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
					<small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
				</div>
				<button type="submit" class="btn btn-outline-info btn-sm">Save</button>
			</form>
		</div>
	</div>
@endsection