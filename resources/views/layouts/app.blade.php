<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Leave') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/JQuery-3.3.1.min.js')}}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md bg-info navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
	                <img src="{{asset('extra/images/logo2.jpg')}}" alt="Logo" style="width:40px;">
                   Leave System
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @cannot('isNotAssigned')
                    <ul class="navbar-nav mr-auto">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('home')}}">Home</a>
                            </li>
                            <!-- Dropdown -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                   Leaves
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{route('request')}}">Apply for Leave</a>
                                    <a class="dropdown-item" href="{{route('myleaves')}}">My leaves</a>
                                    @can('isAdmin')
                                    <a class="dropdown-item" href="{{route('leaves')}}">Leaves</a>
                                        @endcan
                                </div>
                            </li>
	                        @can('isAdmin')
	                        <li class="nav-item dropdown">
		                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
			                        Users
		                        </a>
		                        <div class="dropdown-menu">
			                        <a class="dropdown-item" href="{{route('users')}}">Users</a>
		                        </div>
	                        </li>
								@endcan
                        </ul>
                    </ul>
@endcannot
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
	                    <form class="form-inline">
		                    <input class="form-control-sm mr-sm-2" type="text" placeholder="Search" id="myInput">
	                    </form>
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{route('userprofile')}}">
                                        <img class="rounded-circle" src="/storage/avatars/{{ Auth::user()->avatar }}" style="height: 50px; width: 50px;" alt="no image" />
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script>
	    $(document).ready(function(){
		    $("#myInput").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#myTable tr").filter(function() {
				    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
		    });
	    });
    </script>
</body>
</html>
