@if(count($errors)>0)
	@foreach($errors->all()  as $error)
		<div id="BodyField">
			<div class="video-field-new">
				<div class="alert alert-danger alert-dismissible">
					<p>Sorry: {{session('error')}}</p>
				</div>
			</div>
		</div>
	@endforeach
@endif

@if(session('success'))
	<div id="BodyField">
		<div class="video-field-new">
			<div class="alert alert-success alert-dismissible">
			<p> {{session('success')}}</p>
		</div>
		</div>
	</div>
@endif

@if(session('error'))
	<div id="BodyField">
		<div class="video-field-new">
			<div class="alert alert-danger alert-dismissible">
			<p>Sorry: {{session('error')}}</p>
			</div>
		</div>
	</div>
@endif

<script>
	$(function() {
		$('.video-field-new').delay(7500).show().fadeOut('slow');
	});
</script>
