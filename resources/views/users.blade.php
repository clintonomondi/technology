@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-header">Users</div>
		<div class="card-body">
			@include('includes.message')
			<table class="table table-striped">
				<thead>
				<th>#</th>
				<th>Name</th>
				<th>Role</th>
				<th>Email</th>
				<th></th>
				<th></th>
				<th></th>
				</thead>
				<tbody id="myTable">
				@if(count($users)>0)
					@foreach($users as $user)
						<tr>
							<td>{{$user->id}}</td>
							<td>{{$user->name}}</td>
							@if(($user->user_type)=='Not assigned')
								<td style="color: red;">{{$user->user_type}}</td>
                            @else
							<td style="color: green;">{{$user->user_type}}</td>
							@endif
							<td>{{$user->email}}</td>
							<td><img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" style="height: 50px; width: 50px;" alt="no image" /></td>
							<td><a class="fa fa-trash" style="color: red;" href="{{route('removeuser',['id'=>$user->id])}}">Remove</a> </td>
							<td><a class="fa fa-user" style="color: blue;" href="{{route('profile',['id'=>$user->id])}}">Profile</a> </td>

						</tr>
					@endforeach
				@else
					<p>No No user yet</p>
				@endif
				</tbody>
			</table>
		</div>
	</div>
@endsection
