<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script>
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <!-- Custom Theme files -->
        <link href="extra/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
        <link href="extra/css/style.css" type="text/css" rel="stylesheet" media="all">
        <!-- color switch -->
        <link href="extra/css/blast.min.css" rel="stylesheet" />
        <!-- lightbox -->
        <link rel="stylesheet" href="extra/css/lightbox.min.css">
        <!-- portfolio -->
        <link rel="stylesheet" href="extra/css/portfolio.css">
        <!-- font-awesome icons -->
        <link href="extra/css/font-awesome.min.css" rel="stylesheet">

    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <div class="blast-box">
        <div class="blast-frame">
            <p>color schemes</p>
            <div class="blast-colors d-flex justify-content-center">
                <div class="blast-color">#25e0ff</div>
                <div class="blast-color">#feb800</div>
                <div class="blast-color">#f25050</div>
                <div class="blast-color">#18e7d3</div>
                <!-- you can add more colors here -->
            </div>
            <p class="blast-custom-colors">Choose Custom color</p>
            <input type="color" name="blastCustomColor" value="#cf2626">

        </div>
        <div class="blast-icon"><i class="fa fa-cog" aria-hidden="true"></i></div>

    </div>
    <div id="home" class="banner" data-blast="bgColor">
        <!-- header -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light navbar-fixed-top">
            <div class="container">
                <h1 class="wthree-logo">
                    <a href="index.html" id="logoLink" data-blast="color">SonyFirm</a>
                </h1>
                <div class="nav-item  position-relative">
                    <a href="#menu" id="toggle">
                        <span></span>
                    </a>
                    <div id="menu">
                        <ul>
                             @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <li><a data-blast="color" href="{{ url('/home') }}">Home</a></li>
                                    @else
                                        <li><a data-blast="color" href="{{ route('login') }}">Login</a></li>

                                        @if (Route::has('register'))
                                            <li><a data-blast="color" href="{{ route('register') }}">Register</a></li>
                                        @endif
                                    @endauth
                                </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- //header -->
        <!-- banner -->
        <div class="container-fluid">
            <div class="row banner-row">
                <div class="col-lg-8 bg-img text-center">
                    <h3 class="agile-title">Request for your leave.</h3>
                    <div class="bnr-img">
                        <img src="extra/images/logo2.jpg" alt="" class="img-fluid m-auto" />
                    </div>

                </div>
                <div class="col-lg-4  my-auto p-0">

                    <div id="login-row">
                        <h4>Login now!</h4>
                        <div id="login-column">
                            <div class="box">
                                <div class="shape1 shape-bg"></div>
                                <div class="shape2 shape-bg"></div>
                                <div class="shape3 shape-bg"></div>
                                <div class="shape4 shape-bg"></div>
                                <div class="shape5 shape-bg"></div>
                                <div class="shape6 shape-bg"></div>
                                <div class="shape7 shape-bg"></div>
                                <div class="float">
                                    <form class="form" action="#" method="post">
                                        <div class="form-group">
                                            <a class="btn btn-outline-primary btn-sm" href="{{route('login')}}" >Login</a>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Or</label><br>
                                        </div>
                                        <div class="form-group btn-agile">
                                            <a class="btn btn-outline-primary btn-sm" href="{{route('register')}}" >Register</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //banner -->
    </div>

    </body>
    <!-- js -->
    <script src="extra/js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <!--  menu toggle -->
    <script src="extra/js/menu.js"></script>
    <!-- color switch -->
    <script src="extra/js/blast.min.js"></script>
    <!-- light box -->
    <script src="extra/js/lightbox-plus-jquery.min.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="extra/js/scrolling-nav.js"></script>
    <!-- start-smooth-scrolling -->
    <script src="extra/js/move-top.js"></script>
    <script src="extra/js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="extra/js/SmoothScroll.min.js"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="extra/js/bootstrap.js"></script>

</html>
