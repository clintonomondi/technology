@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 pull-left">
	        <h4>DashBoard!</h4>
	                <hr>
                   @can('isAdmin')
                    You are logged in as Admin!
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="card bg-primary">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-check fa-5x" style="color: white;"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">Pending</div>
                                                <div>{{$pending}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('pending')}}">
                                        <div class="card-footer bg-light">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
	                        <div class="col-lg-3 col-md-3">
		                        <div class="card bg-success">
			                        <div class="card-header">
				                        <div class="row">
					                        <div class="col-xs-3">
						                        <i class="fa fa-check fa-5x" style="color: forestgreen;"></i>
					                        </div>
					                        <div class="col-xs-9 text-right">
						                        <div class="huge">Accepted</div>
						                        <div>{{$accepted}}</div>
					                        </div>
				                        </div>
			                        </div>
			                        <a href="{{route('leaves')}}">
				                        <div class="card-footer bg-light">
					                        <span class="pull-left">View Details</span>
					                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					                        <div class="clearfix"></div>
				                        </div>
			                        </a>
		                        </div>
	                        </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="card bg-secondary">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-cut fa-5x" style="color: darkred;"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge">Rejected</div>
                                                <div>{{$rejected}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('leaves')}}">
                                        <div class="card-footer bg-light">
                                            <span class="pull-left">View Details</span>
                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
	                        <div class="col-lg-3 col-md-3">
		                        <div class="card bg-success">
			                        <div class="card-header">
				                        <div class="row">
					                        <div class="col-xs-3"><i class="fa fa-user fa-5x" style="color: blue
								                ;"></i>
					                        </div>
					                        <div class="col-xs-9 text-right">
						                        <div class="huge">Users</div>
						                        <div>{{$users}}</div>
					                        </div>
				                        </div>
			                        </div>
			                        <a href="{{route('users')}}">
				                        <div class="card-footer bg-light">
					                        <span class="pull-left">View Details</span>
					                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					                        <div class="clearfix"></div>
				                        </div>
			                        </a>
		                        </div>
	                        </div>
                            </div>
                       @endcan
                    @can('isStaff')
                       You are logged in as Staff!
						<hr>
                        @endcan

	            @can('isNotAssigned')
					<p style="color: red">You are not assigned in this system,contact system administrator</p>
					@endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
