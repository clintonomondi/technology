@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
	<div class="col-sm-4">
		@include('modal.changerole')
		@include('includes.message')
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">{{$user->name}}</h4>
				<table class="table">

					<tr>
						<td class="fa fa-envelope">Email</td>
						<td>{{$user->email}}</td>
					</tr>
					<tr>
						<td class="fa fa-sync">Pending leave</td>
						<td style="color: blue;">{{$pending}}</td>
					</tr>
					<tr>
						<td class="fa fa-check">Approved leave</td>
						<td style="color: green;">{{$approved}}</td>
					</tr>
					<tr>
						<td class="fa fa-trash">Rejected leave</td>
						<td style="color: darkred;">{{$rejected}}</td>
					</tr>
					<tr>
						<td class="fa fa-trash"> Role</td>
						@if(($user->user_type)=='Not assigned')
						<td style="color: red;">{{$user->user_type}}</td>
							@else
							<td style="color: green;">{{$user->user_type}}</td>
						@endif
					</tr>

					</tbody>
				</table>
				<a href="#" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#changeRole">Change role</a>
			</div>
			<img class="card-img-top" src="/storage/avatars/{{ $user->avatar }}" alt="Profile image" style="width:100%; height: 200px;">
		</div>
		<br>
	</div>
</div>
@endsection
