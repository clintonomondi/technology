<!-- The Modal -->
<div class="modal fade" id="changeRole">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Change Role</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form method="post" action="{{route('postrole')}}">
					@csrf
					<div class="form-group">
						<label for="sel1">Select role:</label>
						<select class="form-control" id="sel1" name="user_type">
							<option value="{{$user->user_type}}">{{$user->user_type}}</option>
							<option value="staff">Normal staff</option>
							<option value="admin">Admin</option>
							<option value="Not assigned">Unassigned</option>
						</select>
						<input type="text" name="id" value="{{$user->id}}" hidden>
					</div>
					<button type="submit" class="btn btn-outline-info btn-sm">Submmit</button>
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">

			</div>

		</div>
	</div>
</div>