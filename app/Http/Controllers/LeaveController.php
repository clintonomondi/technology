<?php

namespace App\Http\Controllers;

use App\Leave;
use App\Mail\ApprovedLeave;
use App\Mail\RejectedLeave;
use App\Mail\RequestedLeave;
use App\Staff;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Stmt\Return_;


class LeaveController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function request() {
		$leave = User::find(Auth::user()->id)->leave()->where('status', 'Pending')->count();
		return view('leaves.request', compact('leave'));
	}

	public function postrequest(Request $request) {
		$type=$request->input('type');
		$leave = User::find(Auth::user()->id)->leave()->where('status', 'Pending')->where('type',$type)->count();
		if ($leave > 0) {
			return redirect()->back()->with('error', 'You cannot request for another leave at this time');
		}
		$startdate = strtotime($request->input('startdate'));
		$enddate = strtotime($request->input('enddate'));
		$diff = abs($enddate - $startdate) / 60 / 60 / 24;

		if ($diff >= 30 && $type == "Maternal" || $type == "Education") {
			$requestAll = $request->all();
			$requestAll['user_id'] = Auth::user()->id;
			Leave::create($requestAll);

			Mail::to('clintonmbaty@gmail.com')
				->send(new RequestedLeave());
			return redirect()->back()->with('success', 'Leave request submitted');

		} elseif ($diff < 30 && $type != "Maternal") {
			$requestAll = $request->all();
			$requestAll['user_id'] = Auth::user()->id;
			Leave::create($requestAll);

			Mail::to('clintonmbaty@gmail.com')
				->send(new RequestedLeave());
			return redirect()->back()->with('success', 'Leave request submitted');

		} else {
			return redirect()->back()->with('error', 'Leave days exhausted');
		}
	}


	public function leaves() {
		$leaves = Leave::orderBy('id', 'desc')->paginate(10);

		return view('leaves.index', compact('leaves'));
	}

	public function showrequest($id) {
		$leave = Leave::find($id);
		$sender = Leave::find($id)->user;
		return view('leaves.showrequest', compact('leave', 'sender'));
	}

	public function accept($id) {
		$leave = Leave::find($id);
		$sender = Leave::find($id)->user->email;
		$status = 'Accepted';

		$leave->status = $status;
		$leave->save();

		Mail::to($sender)
			->send(new ApprovedLeave());
		return redirect()->back()->with('success', 'leave approved successfully!');
	}

	public function reject($id) {
		$leave = Leave::find($id);
		$sender = Leave::find($id)->user->email;
		$status = 'Rejected';

		$leave->status = $status;
		$leave->save();

		Mail::to($sender)
			->send(new RejectedLeave());
		return redirect()->back()->with('success', 'leave !');
	}

	public function myleaves() {
		$leaves = User::find(Auth::user()->id)->leave()->orderBy('status', 'asc')->paginate(10);
		return view('leaves.myleaves', compact('leaves'));
	}


	public function pending() {
		$leaves = Leave::where('status', 'Pending')->orderBy('created_at', 'asc')->paginate(10);
		return view('leaves.pending', compact('leaves'));
	}

	public function accepted() {
		$leaves = Leave::where('status', 'Accepted')->orderBy('created_at', 'desc')->paginate(10);
		return view('leaves.accepted', compact('leaves'));
	}

	public function rejected() {
		$leaves = Leave::where('status', 'Rejected')->orderBy('created_at', 'desc')->paginate(10);
		return view('leaves.rejected', compact('leaves'));
	}

}
