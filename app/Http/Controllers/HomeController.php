<?php

namespace App\Http\Controllers;

use App\Leave;
use App\Mail\RejectedLeave;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @method uploadOne($image, string $folder, string $string, string $name)
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$accepted=Leave::where('status','Accepted')->count();
	    $rejected=Leave::where('status','Rejected')->count();
	    $pending=Leave::where('status','Pending')->count();
	    $users=User::all()->count();
        return view('home',compact('accepted','rejected','users','pending'));
    }

    public  function users(){
    	$users=User::all();
    	return view('users',compact('users'));
    }

    public  function removeuser($id){
    	$user=User::find($id);
    	$user->delete();
    	return redirect()->back()->with('success','user removed successfully');
    }

    public  function profile($id){
    	$user=User::findorFail($id);
    	$approved=User::find($id)->leave()->where('status','Approved')->count();
    	$rejected=User::find($id)->leave()->where('status','Rejected')->count();
    	$pending=User::find($id)->leave()->where('status','Pending')->count();
    	return view('profile',compact('user','approved','rejected','pending'));
    }

    public  function postrole(Request $request){
    	$id=$request->input('id');
    	$user_type=$request->input('user_type');
    	$user=User::find($id);
    	$user->user_type=$user_type;
    	$user->save();
    	return redirect()->back()->with('succes','User role changed successfully');
    }

    public  function userprofile(){
    	$user=User::find(Auth::user()->id);
    	return view('userprofile',compact('user'));
    }


    public  function updateprofile(Request $request){
	    $request->validate([
		    'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
	    ]);
       //get file name with extension
	    $fileNameWithExt=$request->file('avatar')->getClientOriginalName();
	    $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
	    $extesion=$request->file('avatar')->getClientOriginalExtension();
	    //filename to store
	    $fileNameToStore=$filename.'_'.time().'.'.$extesion;
	    //uploadimage
	    $path=$request->file('avatar')->storeAs('/public/avatars',$fileNameToStore);

	    $user=User::find(Auth::user()->id);
	    $user->avatar=$fileNameToStore;
	    $user->save();
	    return redirect()->back()->with('success','Profile pic updated successfully');
    }
}
