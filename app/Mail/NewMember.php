<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMember extends Mailable
{
    use Queueable, SerializesModels;

	protected $member;

	public function __construct($member)
	{
		$this->member = $member;
	}

    public function build()
    {
        return $this->markdown('emails.members.applicants');
    }
}
