<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovedLeave extends Mailable
{
    use Queueable, SerializesModels;

//	protected $leave;

	public function __construct()
	{
//		$this->leave = $leave;
	}


    public function build()
    {
        return $this->markdown('emails.leaves.aproved');
    }
}
